# mycustodian
My Cloud Custodian policies

## Synopsis
Use this tool to check and remidiate AWS resources, based on the policies defined.

## Policies
Policies are in yaml format

## To setup
```
sudo apt install virtualenv
virtualenv --python=python2 custodian
```

## To run
```
source custodian/bin/activate
cd custodian/
./run_compliance_check.sh
```
## Links
* https://capitalone.github.io/cloud-custodian/docs/ 
* https://gitter.im/capitalone/cloud-custodian 