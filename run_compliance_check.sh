for i in $(aws ec2 describe-regions --query="Regions[].RegionName")
do 
    echo "region $i"
    custodian run --dryrun -s cust compliance.yml --region $i
done
